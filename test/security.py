#!/usr/bin/env python3
import sys

if len(sys.argv) > 1:
    DOCKERFILE=sys.argv[1]
else:
    sys.exit("[e] Usage: "+sys.argv[0]+"[Dockerfile path]")

if 'environment:' in open(DOCKERFILE).read():
    sys.exit("[e] Secret is exposed in an environment variable")

if 'secrets:' not in open(DOCKERFILE).read():
    sys.exit("[e] Secret are not transmited or stored securely")
